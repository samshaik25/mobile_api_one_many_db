
module.exports = app => {
    const mobiles = require("../controllers/mobile.controller.js");
  
    var router = require("express").Router();
  
    router.get("/brands",mobiles.getCompany)
  
    router.get("/products",mobiles.getProducts)



    router.get("/brandAndproduct", mobiles.findAll);


    router.post("/", mobiles.createMobileCompany);

  
    router.post("/product/:id",mobiles.createProduct)

    
    router.get("/:id",mobiles.findMobileCompanyById)


    router.get("/product/:id",mobiles.findProductById)


    router.put("/:id", mobiles.updateCompany);


    router.put("/product/:id",mobiles.updateProduct)


    router.delete("/:id", mobiles.deleteCompany);


    router.delete("/product/:id", mobiles.deleteProduct);

  
    app.use('/api/mobiles', router);
  };
  