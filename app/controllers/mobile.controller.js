
const { products } = require("../models");
const db = require("../models");
const Mobile = db.mobiles;
const Product = db.products;


exports.createMobileCompany = (req,res) => {
  if (!req.body.Company) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a MobileCompany
  const mobile = {
   Company : req.body.Company,
   CEO: req.body.CEO,
   Headquaters: req.body.Headquaters,
   Revenue:req.body.Revenue

  };
  Mobile.create(mobile)
  .then(data => {
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while creating the mobileCompany."
    });
  });
  };


  /////////////////     .........  Create a Product .........      /////////////////////


  exports.createProduct=(req,res)=>{
    if (!req.body.Mobile_Name) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
    const product = {
      Mobile_Name : req.body.Mobile_Name,
      Price: req.body.Price,
      RAM: req.body.RAM,
      Internal_Storage:req.body.Internal_Storage,
      mobileCompanyId:req.params.id
 
     };
   
     Product.create(product)
       .then(data => {
         res.send(data);
       })
       .catch(err => {
         res.status(500).send({
           message:
             err.message || "Some error occurred while creating the Product."
         });
       });
  }

////////////////////////////  ........... Find Company By Id .....  ////////////////////////


  exports.findMobileCompanyById=(req,res)=>{
    const id = req.params.id;
  
    Mobile.findByPk(id,{include:["products"]})
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find MobileComapany with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving MobileComapany with id=" + id
        });
      });
  }


           /////////////////     ..........  Find Product By Id  ......... /////////////////


  exports.findProductById=(req,res)=>{
    const id = req.params.id;
  
    Product.findByPk(id,{include:["mobileCompany"]})
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Product with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Product with id=" + id
        });
      });
  }
 

          ////////  Find all Companies        ///////


          exports.getCompany = (req, res) => {
            Mobile.findAll()
              .then(data => {
                res.send(data);
              })
              .catch(err => {
                res.status(500).send({
                  message:
                    err.message || "Some error occurred while retrieving Mobiles."
                });
              });
          };



          ////////////  Find all Products  //////////


          exports.getProducts = (req, res) => {
            Product.findAll()
              .then(data => {
                res.send(data);
              })
              .catch(err => {
                res.status(500).send({
                  message:
                    err.message || "Some error occurred while retrieving Mobiles."
                });
              });
          };



                  ////////////////// .............   Find ALL  ........  ///////////////////



   exports.findAll = (req, res) => {
    Mobile.findAll({include:["products"]})
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Mobiles."
        });
      });
  };


              //////////////// ...........  Update in Company  .........////////////////


  exports.updateCompany = (req, res) => {
    const id = req.params.id;
  
    Mobile.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        // console.log(num
        if (num == 1) {
          res.send({
            message: "Company was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Company with id=${id}. Maybe COmpany was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Company with id=" + id
        });
      });
  };
  

                   /////////// ....... Update In Product  ..... ////////////


exports.updateProduct= (req,res)=>{
  const id = req.params.id;
  
    Product.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Product was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Product with id=${id}. Maybe Product was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Product with id=" + id
        });
      });
}



       /////////////    ..... Delete a  Company By Id ........... //////////////

  
exports.deleteCompany=(req,res)=>{
  const id=req.params.id;
  Mobile.destroy({where:{id:id}})
  .then(data=>{
    if(data==1){
      res.send({
        message:`Company deleted succesfully with id: ${id}`
      })
    } 
    else{
      res.send({
        message:`Cannot Delete Company with id: ${id} may be Company was not found`
      })
    }
  })
  .catch(err => {
    res.status(500).send({
      message: "Could not delete Comapny with id=" + id
    });
  });
}



             ////////// ...........   Delete Product By Id  ...........//////////////


exports.deleteProduct=(req,res)=>{
  const id=req.params.id;
  Product.destroy({where:{id:id}})
  .then(data=>{
    if(data==1){
      res.send({
        message:`Product deleted succesfully with id: ${id}`
      })
    } 
    else{
      res.send({
        message:`Cannot Delete Product with id: ${id} may be Product was not found`
      })
    }
  })
  .catch(err => {
    res.status(500).send({
      message: "Could not delete Product with id=" + id
    });
  });
}