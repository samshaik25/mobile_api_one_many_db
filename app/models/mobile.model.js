module.exports = (sequelize, DataTypes) => {
    const MobileCompanies = sequelize.define("mobileCompanies", {
      Company: {
        type: DataTypes.STRING
      },
      CEO: {
        type: DataTypes.STRING
      },
      Headquaters:{
        type: DataTypes.STRING

      },
      Revenue:{
        type: DataTypes.STRING
      },

    },{ timestamps:false});
  
    return MobileCompanies;
  };