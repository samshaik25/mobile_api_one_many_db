module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define("product", {
        Mobile_Name: {
            type: DataTypes.STRING,
            
          },
          Price: {
            type: DataTypes.INTEGER,
        
          },
          RAM: {
            type: DataTypes.STRING,
              
          },
          
           Internal_Storage:
           {
               type:DataTypes.STRING  
           }     
    },{ timestamps:false});
  
    return Product;
  };
  