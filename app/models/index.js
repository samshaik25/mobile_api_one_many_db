const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.URL, {
  dialect: dbConfig.dialect,
  operatorsAliases: 0,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.mobiles = require("./mobile.model.js")(sequelize, Sequelize);
db.products = require("./products.model.js")(sequelize, Sequelize);

db.mobiles.hasMany(db.products, { as: "products" });
db.products.belongsTo(db.mobiles, 
  {as:"mobileCompany"},{
  foreignKey: "mobileCompanyId",
  
});

module.exports = db;
